package GoSSHConn

import (
	"fmt"
	"log"
	"net"
	"os"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

func SetupSsh(sshHost string, sshPort int, sshUser string, sshPass string) *ssh.Client {

	pemBytes, err := os.ReadFile("./id_rsa")
	if err != nil {
		log.Fatal(err)
		panic(err)
	}

	// pemBytes := []byte(os.Getenv("SSH_PKEY"))
	signer, err := ssh.ParsePrivateKey(pemBytes)

	if err != nil {
		log.Fatalf("parse key failed:%v", err)
		panic(err)
	}

	var agentClient agent.Agent
	// Establish a connection to the local ssh-agent
	if conn, err := net.Dial("unix", os.Getenv("SSH_AUTH_SOCK")); err == nil {
		defer conn.Close()

		// Create a new instance of the ssh agent
		agentClient = agent.NewClient(conn)
	}

	// The client configuration with configuration option to use the ssh-agent
	sshConfig := &ssh.ClientConfig{
		User:            sshUser,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
	}

	// When the agentClient connection succeeded, add them as AuthMethod
	if agentClient != nil {
		sshConfig.Auth = append(sshConfig.Auth, ssh.PublicKeysCallback(agentClient.Signers))
	}
	// When there's a non empty password add the password AuthMethod
	if sshPass != "" {
		sshConfig.Auth = append(sshConfig.Auth, ssh.PasswordCallback(func() (string, error) {
			return sshPass, nil
		}))
	}

	// Connect to the SSH Server
	sshcon, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", sshHost, sshPort), sshConfig)

	if err != nil {
		log.Fatalf("Failed to connect to the ssh client: %s\n", err.Error())
		panic(err)
	} else {
		return sshcon
	}

}
